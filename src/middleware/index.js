const data = require('../data/data-source');

function isAdmin(req, res, next) {
    const { id } = req.body;
    if (id != undefined) {
        admin_user = data.users.filter((user) => { return user.id == id && user.admin == true });

        if (admin_user.length == 0) {
            res.status(403).json("Usuario no encontrado").end();
        } else {
            next();
        }

    } else {
        res.status(403).json("Debe proveer sus credenciales de admin para acceder a este recurso").end();
    }

};

function isLogged(req, res, next) {
    const { id } = req.body;
    if (id != undefined) {
        logged_user = data.users.filter((user) => { return user.id == id });

        if (logged_user.length == 0) {
            res.status(403).json("El usuario no existe").end();
        } else {
            next();
        }

    } else {
        res.status(403).json("Debe proveer sus credenciales para acceder a este recurso").end();
    }
};

module.exports = {
    isAdmin: isAdmin,
    isLogged: isLogged
}