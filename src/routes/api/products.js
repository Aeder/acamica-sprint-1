const express = require('express');
var router = express.Router();
const data = require('../../data/data-source');
const middleware = require('../../middleware/index');


/**
 * @swagger
 * /api/products:
 *   get:
 *     summary: Obtener una lista de los productos disponibles.
 *     description: Devuelve una lista de todos los usuarios disponibles en la fuente de información.
 *     responses:
 *       200:
 *         description: Una lista de productos.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       id:
 *                         type: integer
 *                         description: ID del producto.
 *                         example: 0
 *                       name:
 *                         type: string
 *                         description: Nombre del producto.
 *                         example: Mermelada Arcor.
 *                       category:
 *                         type: string
 *                         description: Descripción del producto.
 *                         example: Mermelada de frutas envasada.
 *                       price:
 *                         type: float
 *                         description: Precio del producto.
 *                         example: 132.98                   
 */
router.get('/', (req, res) => {
    res.json(data.products);
})

/**
 * @swagger
 * /api/products:
 *   post:
 *     summary: Crea un nuevo producto en la lista de productos
 *     description: Permite a un usuario administrador crear un nuevo producto en la lista
 *     parameters:
 *       - name: id
 *         in: formData
 *         description: ID del usuario que esta agregando el prodcuto.
 *         required: true
 *         type: integer
 *       - name: name
 *         in: formData
 *         description: Nombre del producto.
 *         required: true
 *         type: string
 *       - name: category
 *         in: formData
 *         description: Categoria del producto.
 *         required: true
 *         type: string
 *       - name: price
 *         in: formData
 *         description: Precio del producto.
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: El producto que se ha insertado.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 id:
 *                   type: integer
 *                   description: ID del producto.
 *                   example: 0
 *                 name:
 *                   type: string
 *                   description: Nombre del producto.
 *                   example: Mermelada Arcor.
 *                 category:
 *                   type: string
 *                   description: Descripción del producto.
 *                   example: Mermelada de frutas envasada.
 *                 price:
 *                   type: float
 *                   description: Precio del producto.
 *                   example: 132.98     
 */
router.post('/', middleware.isAdmin, (req, res) => {
    const product_data = req.body;
    console.log(req.body);

    new_product = data.addProduct(product_data)

    res.send(new_product);

})

/**
 * @swagger
 *
 * /api/products/{id}:
 *   patch:
 *     summary: Modifica producto en la lista de productos
 *     description: Permite a un usuario administrador modificar producto en la lista
 *     parameters:
 *       - name: id
 *         in: path
 *         description: ID del producto a modificar
 *         required: true
 *         type: integer
 *       - name: id
 *         in: formData
 *         description: ID del usuario que esta agregando el prodcuto.
 *         required: true
 *         type: integer
 *       - name: name
 *         in: formData
 *         description: Nombre del producto.
 *         required: false
 *         type: string
 *       - name: category
 *         in: formData
 *         description: Categoria del producto.
 *         required: false
 *         type: string
 *       - name: price
 *         in: formData
 *         description: Precio del producto.
 *         required: false
 *         type: number
 *     responses:
 *       200:
 *         description: El producto que se ha modificado.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 id:
 *                   type: integer
 *                   description: ID del producto.
 *                   example: 0
 *                 name:
 *                   type: string
 *                   description: Nombre del producto.
 *                   example: Mermelada Arcor.
 *                 category:
 *                   type: string
 *                   description: Descripción del producto.
 *                   example: Mermelada de frutas envasada.
 *                 price:
 *                   type: float
 *                   description: Precio del producto.
 *                   example: 132.98
 */
router.patch('/:id', middleware.isAdmin, (req, res) => {
    const product_new_data = req.body;

    product_index = data.products.findIndex((elem) => {
        return elem.id == req.params.id;
    });

    console.log(`Product found: ${data.products[product_index]}`);

    Object.keys(product_new_data).forEach(key => {
        data.products[product_index][key] = product_new_data[key];
    })

    res.json(data.products[product_index]);
})

/**
 * @swagger
 *
 * /api/products/{id}:
 *   delete:
 *     summary: Borra un producto en la lista de productos
 *     description: Permite a un usuario administrador modificar producto en la lista
 *     parameters:
 *       - name: id
 *         in: path
 *         description: ID del producto a borrar
 *         required: true
 *         type: integer
 *       - name: id
 *         in: formData
 *         description: ID del usuario con privilegios de administrador
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: Respuesta existosa
 */

router.delete('/:id', middleware.isAdmin, (req, res) => {

    let new_product_list = data.products.filter((product) => {
        return product.id != req.params.id;
    });

    data.products = new_product_list;

    res.send("Producto borrado");
});

module.exports = router;