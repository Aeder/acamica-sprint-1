const express = require('express');
const router = express.Router();
const data = require('../../data/data-source');
const middleware = require('../../middleware/index');


/**
 * @swagger
 *
 * /api/orders:
 *   get:
 *     summary: Permite ver la lista completa de pedidos
 *     description: Permite a un usuario administrador ver la lista completa de pedidos realizados. Para probarlo debe emplearse Postman ya que la implementación de GET de Swagger es incompleta y no permite parametros en Body como la especificacion oficial de HTTP dicta.
 *     parameters:
 *       - name: id
 *         in: formData
 *         description: ID del usuario administrador
 *         type: integer
 *     responses:
 *       200:
 *         description: Lista de todos los pedidos
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       id:
 *                         type: integer
 *                         description: ID del pedido
 *                         example: 0
 *                       hora:
 *                         type: integer
 *                         description: Timestamp de la fecha y hora del pedido 
 *                         example: 324534545634
 *                       price:
 *                         type: number
 *                         description: Precio total del pedido
 *                         example: 200.50
 *                       state:
 *                         type: string
 *                         description: Estado del pedido
 *                         example: CONFIRMED
 *                       payment_method:
 *                         type: string
 *                         description: Metodo de pago
 *                         example: DEBIT
 *                       adress:
 *                         type: string
 *                         description: Direccion opcional del pedido
 *                         example:
 *                       user:
 *                         type: integer
 *                         description: ID del usuario que hizo el pedido
 *                         example: 0
 *                       products:
 *                         type: array
 *                         items:
 *                           type: object
 *                           properties:
 *                             id:
 *                               type: integer
 *                               description: ID del producto
 *                               example: 0
 *                             amount:
 *                               type: integer
 *                               description: Cantidad del producto
 *                               example: 10
 *          
 */

router.get('/', middleware.isAdmin, (req, res) => {
  res.json(data.orders);
})

/**
 * @swagger
 *
 * /api/orders/{id}:
 *   get:
 *     summary: Permite a un usuario ver su historial de pedidos.
 *     description: Permite a un usuario ver todos los pedidos que ha realizado.
 *     parameters:
 *       - name: id
 *         in: formData
 *         description: ID del usuario.
 *         type: integer
 *     responses:
 *       200:
 *         description: Lista de todos los pedidos
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       id:
 *                         type: integer
 *                         description: ID del pedido
 *                         example: 0
 *                       hora:
 *                         type: integer
 *                         description: Timestamp de la fecha y hora del pedido 
 *                         example: 324534545634
 *                       price:
 *                         type: number
 *                         description: Precio total del pedido
 *                         example: 200.50
 *                       state:
 *                         type: string
 *                         description: Estado del pedido
 *                         example: CONFIRMED
 *                       payment_method:
 *                         type: string
 *                         description: Metodo de pago
 *                         example: DEBIT
 *                       adress:
 *                         type: string
 *                         description: Direccion opcional del pedido
 *                         example:
 *                       user:
 *                         type: integer
 *                         description: ID del usuario que hizo el pedido
 *                         example: 0
 *                       products:
 *                         type: array
 *                         items:
 *                           type: object
 *                           properties:
 *                             id:
 *                               type: integer
 *                               description: ID del producto
 *                               example: 0
 *                             amount:
 *                               type: integer
 *                               description: Cantidad del producto
 *                               example: 10
 * 
 */

router.get('/history/:id', middleware.isLogged, (req, res) => {
  let user_id = parseInt(req.params.id);
  console.log(`GET /orders/history/${user_id}`)
  let user_history = data.orders.filter((order) => { return user_id == order.user });
  res.json(user_history)
})

/**
 * @swagger
 * 
 * /api/orders:
 *   post:
 *     summary: Permite al usuario realizar un pedido
 *     description: Permite al usuario ingresar los parametros necesarios para realizar un pedido.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/OrderDetails'
 *     responses:
 *       200:
 *         description: Pedido creado
 *
 * components:
 *   schemas:
 *     OrderDetails:
 *       type: object
 *       required:
 *         - id
 *         - payment_method
 *         - products
 *       properties:
 *         id:
 *           type: integer
 *         payment_method:
 *           type: string
 *           enum: ["CREDIT", "DEBIT"]
 *         address:
 *           type: string
 *         products:
 *           type: array
 *           items:
 *             type: object
 *             properties:
 *              id:
 *                type: integer
 *                description: ID del producto
 *                example: 0
 *              amount:
 *                type: integer
 *                description: Cantidad del producto
 *                example: 10            
 */

router.post('/', middleware.isLogged, (req, res) => {
  const order_data = req.body;

  result = data.addOrder(order_data.payment_method,
    order_data.address,
    order_data.id,
    order_data.products
  )

  if (result[0]) {
    res.status(201).send(result[1]);
  } else {
    res.status(400).send(result[1]);
  }
})

/**
 * @swagger
 *
 * /api/orders/{id}:
 *   patch:
 *     summary: Permite a un administrador modificar un pedido
 *     description: Permite a un administrador modificar los parametros de un pedido.
 *     parameters:
 *       - name: id
 *         in: path
 *         description: ID del pedido.
 *         type: integer
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/CompleteOrderDetails'
 *     responses:
 *       200:
 *         description: Pedido modificado
 *
 * components:
 *   schemas:
 *     CompleteOrderDetails:
 *       type: object
 *       required:
 *         - id
 *       properties:
 *         id:
 *           type: integer
 *         data:
 *           type: object
 *           properties:
 *             payment_method:
 *               type: string
 *               enum: ["CREDIT", "DEBIT"]
 *             address:
 *               type: string
 *             products:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   id:
 *                     type: integer
 *                     description: ID del producto
 *                     example: 0
 *                   amount:
 *                     type: integer
 *                     description: Cantidad del producto
 *                     example: 10            
 */

router.patch('/:id', middleware.isAdmin, (req, res) => {
  const order_new_data = req.body.data;

  console.log(order_new_data)

  order_index = data.orders.findIndex((elem) => {
    return elem.id == parseInt(req.params.id);
  });

  console.log(`Product found: ${data.orders[order_index]}`);

  Object.keys(order_new_data).forEach(key => {
    data.orders[order_index][key] = order_new_data[key];
  })

  res.json(data.orders[order_index]);

})

module.exports = router;