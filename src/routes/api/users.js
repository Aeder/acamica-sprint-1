const express = require('express');
var router = express.Router();
const data = require('../../data/data-source');


/**
 * @swagger
 *
 * /api/users:
 *   post:
 *     summary: Crea un nuevo usuario
 *     description: Permite la creación de un usuario común.
 *     parameters:
 *      - name: username
 *        in: formData
 *        description: El nombre de la cuenta de usuario
 *        required: true
 *        type: string
 *      - name: full_name
 *        in: formData
 *        description: El nombre completo del usuario
 *        required: true
 *        type: string 
 *      - name: phone
 *        in: formData
 *        description: El numero de telefono del usuario
 *        required: true
 *        type: integer
 *      - name: email
 *        in: formData
 *        description: El email del usuario
 *        required: true
 *        type: string
 *      - name: address
 *        in: formData
 *        description: La direccion fisica del usuario
 *        required: true
 *        type: string
 *      - name: password
 *        in: formData
 *        description: La contraseña deseada para la cuenta de usuario
 *        required: true
 *        type: string
 *     responses:
 *       200:
 *         description: Usuario creado existosamente
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 id:
 *                   type: integer
 *                   description: ID del usuario creado.
 *                   example: 0
 *                 username:
 *                   type: string
 *                   description: Nombre de la cuenta de usuario creada.
 *                   example: SuperSancho.
 *                 full_name:
 *                   type: string
 *                   description: Nombre completo del usuario creado.
 *                   example: Sancho Perez.
 *                 phone:
 *                   type: integer
 *                   description: Telefono del usuario creado.
 *                   example: 542964444333
 *                 email:
 *                   type: string
 *                   description: Email del usuario creado.
 *                   example: sancho@gmail.com
 *                 address:
 *                   type: string
 *                   description: Dirección del usuario creado.
 *                   example: Calle Norte 1200, Buenos Aires, Argentina
 *                 password:
 *                   type: string
 *                   description: Contraseña del usuario creado.
 *                   example: 439nvf903
 *                 admin:
 *                   type: boolean
 *                   description: Permisos de administrador de la cuenta creada.
 *                   example: false
 *
 */
router.post('/', (req, res) => {
    const new_user_data = req.body;
    let result = data.addUser(new_user_data);
    if (result) {
        res.json(result);
    } else {
        res.status(400).json("El email seleccionado ya esta en uso")
    }
})

/**
 * @swagger
 *
 * /api/users/login:
 *   post:
 *     summary: Permite al usuario ingresar a su cuenta.
 *     description: Permite al usuario ingresar a su cuenta con el nombre de la misma, y devuelve el id a usar como identificacion en otros endpoints.
 *     parameters:
 *     - name: request_username
 *       in: formData
 *       description: El nombre de la cuenta de usuario
 *       required: true
 *     - name: request_password
 *       in: formData
 *       description: La contraseña de la cuenta de usuario
 *       required: true
 *     responses:
 *       200:
 *         description: Usuario creado existosamente
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 id:
 *                   type: integer
 *                   description: ID del usuario que ingreso al sistema.
 *                   example: 0
 */

router.post('/login', (req, res) => {
    const { request_username, request_password } = req.body;
    console.log(req.body);

    let selected_user = data.users.filter((user) => {
        return user.username == request_username;
    });
    // Si la longitud del array resultante es 0 no existen usuarios con ese nombre
    if (selected_user.length == 0) {
        res.status(404).send("No existe el usuario");
    } else {
        if (request_password != selected_user[0].password) {
            res.status(403).send("Contraseña incorrecta");
        } else {
            res.status(200).json({ id: selected_user[0].id });
        }
    }

});

module.exports = router;