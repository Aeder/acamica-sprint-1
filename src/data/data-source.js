// Orders data
const order_states = {
    PENDING: "Pendiente",
    CONFIRMED: "Confirmado",
    PROCESSING: "En preparación",
    SENT: "Enviado",
    DELIVERED: "Entregado"
}

const payment_methods = {
    CREDIT: "Credito",
    DEBIT: "Debito"
}

var last_order_id = 0

var orders = [
    {
        hora: Date.parse('2011-07-10T14:48:00'),
        id: 0,
        price: 0,
        state: "PENDING",
        payment_method: "CREDIT",
        address: "Dirección",
        user: 0,
        products: [
            { id: 0, amount: 10 }
        ]
    }
];

let addOrder = (payment_method, address, user, order_products) => {

    if (payment_method in payment_methods) {
        last_order_id++

        let final_price = 0.0;

        if (products.length > 0) {
            order_products.forEach(order_product => {
                let found_product = products.find(product => product.id == order_product.id);
                final_price += (parseFloat(found_product.price) * parseFloat(order_product.amount))

            });
        }

        console.log(final_price);

        new_order = {
            hora: Date.now(),
            id: last_order_id,
            price: final_price.toFixed(2),
            state: "PENDING",
            payment_method: payment_methods[payment_method],
            address: address,
            user: user,
            products: order_products
        }

        orders.push(new_order);

        return [true, new_order];
    } else {
        return [false, "Metodo de pago invalido"];
    }
}

// Products data

var product_counter = 0;

var products = [
    {
        id: 0,
        name: "Whiskas",
        category: "Alimento para mascotas",
        price: 19.99
    }
];

let addProduct = (product) => {
    product_counter++;

    new_product = {
        id: product_counter,
        name: product.name,
        category: product.category,
        price: product.price
    };

    products.push(new_product);

    return new_product;

}


// Users data

var users_counter = 1;

var users = [
    {
        id: 0,
        username: "admin",
        full_name: "Jose Admin",
        phone: 02604433271,
        email: "admin@acamica-sprint.com",
        address: "San Martin 258, San Rafael, Mendoza",
        password: "password123",
        admin: true
    }
];

let addUser = (user_data) => {

    // Checking that the email is not in use
    let user_check = users.filter((user) => {
        return user.email == user_data.email;
    })

    if(user_check.length > 0){
        console.log("addUser: User already exists")
        return;
    }

    users_counter++;

    let new_user = {
        id: users_counter,
        username: user_data.username,
        full_name: user_data.full_name,
        phone: parseInt(user_data.phone),
        email: user_data.email,
        address: user_data.address,
        password: user_data.password,
        admin: false
    }

    users.push(new_user);

    return new_user;
}

module.exports = {
    orders: orders,
    order_states: order_states,
    products: products,
    users: users,
    addUser: addUser,
    addOrder: addOrder,
    addProduct: addProduct
}