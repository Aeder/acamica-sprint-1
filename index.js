const express = require("express");
const app = express();
const swaggerUi = require('swagger-ui-express');
const swaggerJSDoc = require('swagger-jsdoc');

app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

const swaggerOptions = {
    swaggerDefinition: {
        openapi: "3.0.0",
        components: {},
        info: {
            title: 'Acamica Sprint 1 API',
            version: '1.0.1'
        }
    },
    apis: [
        './src/routes/api/products.js',
        './src/routes/api/users.js',
        './src/routes/api/orders.js'
    ],
};

const swaggerSpec = swaggerJSDoc(swaggerOptions);

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
app.use('/api', require('./src/routes/api'));

app.listen(5000, () => { console.log("Corriendo en http://127.0.0.1:5000/") })