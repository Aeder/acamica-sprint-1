# acamica-sprint-1

Proyecto para el primer sprint de Acamica

# Instalación en Linux (Fedora)

1. Asegurarse de tener git y npm instalados:

`sudo dnf install npm git` 

2. Ejecutar git clone en la carpeta donde queremos descargar el programa

```
cd mi-carpeta
git clone https://gitlab.com/Aeder/acamica-sprint-1.git
```

3. Instalar las librerias necesarias
```
cd acamica-sprint-1
npm install
```

4. Correr el programa

`npm run test`